param(
	[string]$Source,
	[string]$Dest
)

write-output "Checking if drivers for this model and OS architechture exist on engr-wintools (in `"$Source`")..."
if(Test-Path $Source) {
	write-output "Drivers exist."

# Add custom DevicePath to registry
	# This is where Windows will look for drivers when searching
	$regkey = "HKLM\Software\Microsoft\Windows\CurrentVersion"
	$regvalue = "C:\Windows\inf;$Dest"
	reg add $regkey /v DevicePath /t REG_EXPAND_SZ /d $regvalue /f

	# Copy files from (probably remote) source to (probably local) destination
	write-output "Copying files from `"$Source`" to `"$Dest`"..."
	robocopy /s /e /r:3 /w:5 $Source $Dest

	# Get all infs
	write-output "Getting all INF files, recursively, in `"$Dest`"..."
	$infs = Get-ChildItem -Path $Dest -Filter "*.inf" -Recurse -File

	# Try to install each inf
	write-output "Installing drivers..."
	foreach($inf in $infs) {
		write-output "Installing `"$($inf.Fullname)`"..."
		# https://docs.microsoft.com/en-us/windows-hardware/drivers/devtest/pnputil-command-syntax
		#pnputil /add-driver $inf.Fullname /install
		
		# https://gist.github.com/xee5ch/1021553#file-driverimport-bat
		# Legacy syntax, used in case we're installing on Win7
		pnputil -i -a $inf.Fullname
	}
}
else {
	write-output "Drivers don't exist."
}

write-output "Done."