# Source files
$source = "c:\engrit\scripts\set-browser-settings\browser-settings"

# Chrome
$chromeDir = "$(${env:programfiles(x86)})\Google\Chrome\Application"
if(Test-Path $chromeDir) {
	Copy-Item "$source\chrome\*" $chromeDir -Force
}

# Firefox
$ff32Dir = "$(${env:programfiles(x86)})\Mozilla Firefox"
if(Test-Path $ff32Dir) {
	Copy-Item "$source\ff\*" $ff32Dir -Force
}
$ff64Dir = "$($env:programfiles)\Mozilla Firefox"
if(Test-Path $ff64Dir) {
	Copy-Item "$source\ff\*" $ff64Dir -Force
}