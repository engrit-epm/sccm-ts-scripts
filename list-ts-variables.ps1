$varsInitial = Get-CMTaskSequence -Name "UIUC-ENGR-Basic Win10/7" | Get-CMTaskSequenceStep | Where {$_.VariableName -match "EngrIT_"} | select @{Name='name';Expression={$_.VariableName}},@{Name='valueInitial';Expression={$_.VariableValue}}

$tsenv = New-Object -COMObject Microsoft.SMS.TSEnvironment
$tsenvCopy = $tsenv
$tsenvCopy.getType() | Out-String
$tsenvCopy | ConvertTo-Json

$varsAll = @{}
foreach($var in $varsInitial) {
	$varsAll.Add($var.name, @{
		valueInitial = $var.valueInitial
		valueFinal = $tsenv.Value($var.name)
	})
	#$tsenvCopy.Remove
}

Write-Output " `
=======================
Custom TS Variables
======================="
$varsAll | ConvertTo-Json


Write-Output " `
=======================
Other TS Variables
======================="
$tsenvCopy | ConvertTo-Json