param(
	[switch]$UseServiceAccount,
	[switch]$CalledFromTS,
	[switch]$DebugOutput,
	# Can be "IP" or "MAC"
	# Passing in anything except "MAC" will cause "IP" to be used
	# "IP" is safer because it's less likely to return multiple conflicting results
	[string]$BasedOn="IP",
	[string]$givenMAC=$false,
	[string]$givenIP=$false,
	[string]$LogDir
)

$log = "$LogDir\get-lens-info-script.log"
if($CalledFromTS) {
	new-item -path $log -itemtype "file" -value "Start of log."
}

function debug($msg) {
	if($DebugOutput) {
		Write-Host $msg
		if($CalledFromTS) {
			Write-Output $msg >> $log
		}
	}
}

function debugObject($object) {
	debug($object)
	debug("---------------------")
	debug($object.ip_mac_id)
	debug($object.ip)
	debug($object.mac)
	debug("---")
	debug($object.most_recent)
	debug($object.first_seen)
	debug($object.last_seen)
	debug("---")
	debug($object.device_id)
	debug($object.router_ip)
	debug($object.router_name)
	debug("---")
	debug($object.subnet_id)
	debug($object.subnet)
	debug($object.subnet_name)
	debug("---------------------")
}

function getLocalIP() {
	$ip = 'not set'
	
	if($GivenIP -ne $false) {
		$ip = $GivenIP
	}
	else {
		# https://stackoverflow.com/questions/27277701/powershell-get-ipv4-address-into-a-variable

		# Doesn't work without reliable DNS response
		#$ip = (Test-Connection -ComputerName $env:COMPUTERNAME -Count 1).IPV4Address.IPAddressToString

		# Apparently Get-NetIPConfiguration isn't available in WinPE
		#$ip = (Get-NetIPConfiguration | Where-Object {$_.IPv4DefaultGateway -ne $null -and $_.NetAdapter.Status -ne "Disconnected"}).IPv4Address.IPAddress

		# Let's try ipconfig then
		# https://stackoverflow.com/questions/27277701/powershell-get-ipv4-address-into-a-variable
		$ip = $(ipconfig | where {$_ -match 'IPv4.+\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' } | out-null; $Matches[1])
	}
		
	return $ip
}

function getLocalMAC() {
	$mac = 'not set'
	
	if($GivenMAC -ne $false) {
		$mac = $GivenMAC
	}
	else {
		$mac = ipconfig /all | select-string 'Physical.+\s([0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2})' -allmatches | % {$_.matches.groups[1].value}
	}
		
	return $mac
}

function getLensCreds() {
	$promptForCreds = $false
	$lensUser = 'not set'
	$lensPass = 'not set'
	
	# Get creds
	if($UseServiceAccount) {
		if($CalledFromTS) {
			$lensCredsProtected = $tsenv.Value('EngrIT_LensCreds')
			#debug($lensCredsProtected)
			$lensCredsSplit = $lensCredsProtected.split(';')
			#debug($lensCredsSplit)
			$lensUser = $lensCredsSplit[0]
			#debug($lensUser)
			$lensPass = $lensCredsSplit[1]
			#debug($lensPass)
		}
		else {
			debug("Cannot use service account if not called from TS. Prompting for credentials...")
			$promptForCreds = $true
		}
	}
	else {
		$promptForCreds = $true
	}
	
	if($promptForCreds) {
		$lensCreds = get-credential
		#debug($lensCreds)
		$lensUser = $lensCreds.UserName
		#debug($lensUser)
		$lensPass = $lensCreds.GetNetworkCredential().Password
		#debug($lensPass)
	}
	
	return "${lensUser}:${lensPass}"
}

function getHeaders() {
	# https://stackoverflow.com/questions/27951561/use-invoke-webrequest-with-a-username-and-password-for-basic-authentication-on-t
	# https://answers.uillinois.edu/illinois/page.php?id=47895
	$lensCreds = getLensCreds
	#debug($lensCreds)
    $bytes = [System.Text.Encoding]::ASCII.GetBytes($lensCreds)
	#debug($bytes)
    $base64 = [System.Convert]::ToBase64String($bytes)
	#debug($base64)
    $basicAuthValue = "Basic $base64"
	#debug($basicAuthValue)
    $headers = @{ Authorization = $basicAuthValue }
	#debug($headers)

	return $headers
}

# "cURL-y" stuff goes here
function lensCall($local) {
	$results = 'no result'
	
	# "cURL-ing" in PowerShell
	# https://superuser.com/questions/344927/powershell-equivalent-of-curl
	# https://docs.microsoft.com/en-us/powershell/module/Microsoft.PowerShell.Utility/Invoke-RestMethod?view=powershell-5.1
	# https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/invoke-webrequest?view=powershell-5.1
	# https://discoposse.com/2012/06/30/powershell-invoke-restmethod-putting-the-curl-in-your-shell/
	
	$BasedOnFinal = "ip"
	if($BasedOn -eq "MAC") {
		$BasedOnFinal = "mac"
	}

	$lensUrl = "https://lens-api.cites.illinois.edu/lens/uiuc-lens/ip_mac?${BasedOnFinal}="
	$lensContentType = "&content-type=application/json"
	$requestUrl = $lensUrl + $local.$BasedOnFinal + $lensContentType
	debug($requestUrl)
	
	$headers = getHeaders
	
	$results = Invoke-RestMethod -uri $requestUrl -Headers $headers -Method get
    
	return $results
}

function sanitizeIP($ip) {
	$ip = $ip.replace('[^0-9]', '')
	return $ip
}

function sanitizeMAC($mac) {
	$mac = $mac.ToLower()
	$mac = $mac.replace('-', '')
	$mac = $mac.replace('[^a-f0-9]', '')
	return $mac
}

function ipsAreEqual($a, $b) {
	$a = sanitizeIP($a)
	$b = sanitizeIP($b)
	if($a -eq $b) {
		debug("$a -eq $b")
		return $true
	}
	debug("$a -ne $b")
	return $false
}

function macsAreEqual($a, $b) {
	$a = sanitizeMAC($a)
	$b = sanitizeMAC($b)
	if($a -eq $b) {
		debug("$a -eq $b")
		return $true
	}
	debug("$a -ne $b")
	return $false
}

if($CalledFromTS) {
	# Access to TS variables
	$tsenv = New-Object -COMObject Microsoft.SMS.TSEnvironment
}

$local = @{
	ip = 'not set'
	mac = 'not set'
}

$local.ip = getLocalIP
debug($local.ip)
$local.mac = getLocalMAC
debug($local.mac)

$return = lensCall($local)

# See this link for the anatomy of a Lens API result object:
# https://answers.uillinois.edu/illinois/48350
debug($return)

if($return.error -eq "") {
	$return.error = "None"
}

debug("Status: $($return.status)")
debug("Error: $($return.error)")
debug("Message: $($return.message)")
debug("Result(s): $($return.result)")
debug("Object(s): $($return.objects)")

$numResults = $return.result.length
debug("Number of results: $numResults")

$final = @{
	ip = 'not set'
	mac = 'not set'
	subnet = 'not set'
}

foreach($result in $return.result) {
	$object = $return.objects.ip_mac.$result
	debug($object)
	debug($object.ip)
	debug($object.mac)
	debug($object.subnet_name)
	debug($local.ip)
	debug($local.mac)
	
	# It's possible the Lens call will return multiple results, even multiple conflicting results.
	# To make sure we're getting the most accurate one, look through the results until we find one that matches the IP and MAC that we know are correct.
	if(ipsAreEqual $object.ip $local.ip) {
		if(macsAreEqual $object.mac $local.mac) {
			$final.ip = sanitizeIP($object.ip)
			$final.mac = sanitizeMAC($object.mac)
			$final.subnet = $object.subnet_name
		}
	}
}

debug($final.mac)
debug($final.ip)
debug($final.subnet)

if($CalledFromTS) {
	$tsenv.Value('EngrIT_MAC') = $final.mac
	$tsenv.Value('EngrIT_IP') = $final.ip
	$tsenv.Value('EngrIT_Net') = $final.subnet
}
