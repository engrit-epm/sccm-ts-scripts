param(
	[string]$Source,
	[string]$Dest,
	[int]$DiskToWipe=-1
)

write-output "Checking if WinPE drivers for this model exist on engr-wintools (in `"$Source`")..."
if(Test-Path $Source) {
	write-output "Drivers exist."
	
	# Copy files from (probably remote) source to (probably local) destination
	write-output "Copying files from `"$Source`" to `"$Dest`"..."
	robocopy /s /e /r:3 /w:5 $Source $Dest

	# Get all infs
	write-output "Getting all INF files, recursively, in `"$Dest`"..."
	$infs = Get-ChildItem -Path $Dest -Filter "*.inf" -Recurse -File

	# Try to install each inf
	write-output "Installing drivers..."
	foreach($inf in $infs) {
		write-output "Installing `"$($inf.Fullname)`"..."
		# https://docs.microsoft.com/en-us/windows-hardware/drivers/devtest/pnputil-command-syntax
		pnputil /add-driver $inf.Fullname /install
		
		# https://gist.github.com/xee5ch/1021553#file-driverimport-bat
		# Legacy syntax
		#pnputil -i -a $inf.Fullname
	}

	# Give time for drive to be recognized?
	Start-Sleep -s 15

	# Check to see if drives are recognized
	$diskpartScript = "$Dest\diskpart.txt"

	# For whatever reason, you have to explicitly use ascii when writing script files for diskpart's consumption
	# https://social.technet.microsoft.com/Forums/windows/en-US/de65304a-87cc-431c-b3fe-64170d26355d/diskpart-s-scripttxt-not-working-invalid-commands?forum=ITCG
	"list disk" | out-file $diskpartScript -encoding "ascii"

	# Wipe drive if requested
	if($DiskToWipe -ne -1) {
		"select disk $DiskToWipe" | out-file $diskpartScript -encoding "ascii" -append
		"clean" | out-file $diskpartScript -encoding "ascii" -append
	}
	diskpart /s $diskpartScript 2>&1

	# Give more time for drive to be recognized?
	Start-Sleep -s 15

	# Check to see if c:\ is recognized
	dir c:\ 2>&1
}
else {
	write-output "Drivers don't exist."
}
write-output "Done."