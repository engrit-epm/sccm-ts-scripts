#This REQUIRES PowerShell 5.0
#Parameter Details
#log - log location full path i.e. c:\bin\wuinstall.log
param(
	[string]$moduledir,
	[string]$log="c:\engrit\logs\post-action-script-update-windows-script.log"
)

#Import PSWindowsUpdate module
echo "Importing PSWindowsUpdate module..."
ipmo $moduledir

#Check if Windows Update is a registered service location, register if not present
echo "Getting ServiceID of WUServiceManager to check if service is registered..."
$service = get-wuservicemanager | select-object -property ServiceID

$echoString = "ServiceID: $service"
echo $echoString

echo "Checking if ServiceID matches ID for Microsoft Update (7971f918-a847-4430-9279-4a52d1efe18d)..."
if($service -NotMatch "7971f918-a847-4430-9279-4a52d1efe18d") {
	echo "ServiceID does not match or doesn't exist."
	echo "Registering service..."
	Add-WUServiceManager -ServiceID "7971f918-a847-4430-9279-4a52d1efe18d" -Confirm:$false
}
else {
	echo "ServiceID matched. Presumably this means the service is already registered."
}

#Get all updates that are not drivers or language packs and install, no reboot
echo "Attempting to get and install all updates that are not drivers or language packs..."
Get-WuInstall -MicrosoftUpdate -NotCategory Drivers,Language -NotKBArticleID KB2876229 -AcceptAll -IgnoreReboot -Confirm | Out-File $log -Append
echo "Done with updates."

exit
