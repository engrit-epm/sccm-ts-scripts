param(
	[string]$Source
)
Set-PSDebug -Trace 1
Copy-Item "$Source\StartMenu.bin" "c:\engrit\scripts"
Import-StartLayout -LayoutPath "c:\engrit\scripts\StartMenu.bin" -MountPath $env:SystemDrive\
Write-Output "Done."