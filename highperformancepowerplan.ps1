﻿Try {
        $HighPerf = powercfg -l | %{if($_.contains("High performance")) {$_.split()[3]}}
        $CurrPlan = $(powercfg -getactivescheme).split()[3]
        if ($CurrPlan -ne $HighPerf) {
			powercfg -setactive $HighPerf
			Write-Output "Done."
		}
		else {
			Write-Output "High performance power plan was already set."
		}
    } Catch {
        Write-Output "Unable to set power plan to high performance"
    }