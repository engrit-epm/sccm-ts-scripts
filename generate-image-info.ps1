$tsenv = New-Object -COMObject Microsoft.SMS.TSEnvironment

Set-Content "c:\engrit\image-info.txt" `
"=======================
Image info
=======================
The task sequence run was $($tsenv.Value('_SMSTSPackageName')).
This file was created during imaging on $($tsenv.Value('EngrIT_Timestamp')).

=======================
TS Variables
=======================
EngrIT_isTestTS = $($tsenv.Value('EngrIT_isTestTS'))
EngrIT_isOSDTS = $($tsenv.Value('EngrIT_isOSDTS'))
EngrIT_Timestamp = $($tsenv.Value('EngrIT_Timestamp'))
EngrIT_Model = $($tsenv.Value('EngrIT_Model'))
EngrIT_LogsGoHere = $($tsenv.Value('EngrIT_LogsGoHere'))

=======================
TS Variables (UI++)
=======================
EngrIT_UIPPRan = $($tsenv.Value('EngrIT_UIPPFailed'))
EngrIT_UIPPRanFrom = $($tsenv.Value('EngrIT_UIPPRanFrom'))
EngrIT_OS = $($tsenv.Value('EngrIT_OS'))
EngrIT_ComputerName = $($tsenv.Value('EngrIT_ComputerName'))
EngrIT_AdminNetID = $($tsenv.Value('EngrIT_AdminNetID'))
EngrIT_Office = $($tsenv.Value('EngrIT_Office'))
EngrIT_Origin = $($tsenv.Value('EngrIT_Origin'))
EngrIT_Mathematica = $($tsenv.Value('EngrIT_Mathematica'))
EngrIT_Matlab = $($tsenv.Value('EngrIT_Matlab'))
EngrIT_Autocad = $($tsenv.Value('EngrIT_Autocad'))

=======================
TS Variables (Slack)
=======================
EngrIT_SelectedSoftware = $($tsenv.Value('EngrIT_Mathematica'))
EngrIT_SlackMsgStart = $($tsenv.Value('EngrIT_UIPPFailed'))
EngrIT_SlackMsgMid = $($tsenv.Value('EngrIT_UIPPRanFrom'))
EngrIT_SlackMsgEnd = $($tsenv.Value('EngrIT_OS'))
EngrIT_SlackChannel = $($tsenv.Value('EngrIT_ComputerName'))
EngrIT_SlackName = $($tsenv.Value('EngrIT_AdminNetID'))
EngrIT_SlackIcon = $($tsenv.Value('EngrIT_Office'))
EngrIT_SlackWebhook = $($tsenv.Value('EngrIT_Origin'))

=======================
Collection Variables
=======================
EngrIT_isLoanerLaptop = $($tsenv.Value('EngrIT_isLoanerLaptop'))
EngrIT_isTestMachine = $($tsenv.Value('EngrIT_isTestMachine'))
EngrIT_MechSE_JoeMuskin = $($tsenv.Value('EngrIT_MechSE_JoeMuskin'))

=======================
Built-in Variables
=======================
_SMSTSLaunchMode = $($tsenv.Value('_SMSTSLaunchMode'))
_SMSTSMachineName = $($tsenv.Value('_SMSTSMachineName'))
OSDComputerName = $($tsenv.Value('OSDComputerName'))
"