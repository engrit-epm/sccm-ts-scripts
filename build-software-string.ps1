# Access to TS variables
$tsenv = New-Object -COMObject Microsoft.SMS.TSEnvironment

# Array of possible softwares that can be selected
# Would like to find a way to not have to define this in the script (i.e. pass it from the TS). For now this will work.
# These strings represent the names od the different TS variables populated by the UI++ checkboxes.
# i.e. if you add a variable named "EngrIT_Software" to the TS, then add an array member of "Software".
$availableArray = @(
	'Office',
	'BasicApps',
	'Crowdstrike',
	'Autocad',
	'Mathematica',
	'Matlab',
	'Origin'
)

# Blank array for saving which titles were selected
$selectedArray = @()

# Add each selected title to the new array
foreach($software in $availableArray) {
	if($tsenv.Value("EngrIT_$($software)") -eq "True") {
		$selectedArray += $software
	}
}

# Build the string of selected software
$softwareString = $selectedArray -join ", "

# Use a default string if no titles were selected
if($softwareString -eq "") {
	$softwareString = "none selected"
}

# Save the string to a TS variable
$tsenv.Value('EngrIT_SelectedSoftware') = $softwareString
