﻿# By mseng3

function dumpError($error) {
	Write-Output " "
	
	Write-Output "Error: $error"
	Write-Output " "
	
	$_.PSObject.Properties | ForEach-Object {
		Write-Output "$($error.Name):"
		Write-Output "$($error.Value):"
		Write-Output " "
	}
}

# Regex string for pulling out power plan GUID
$guidRegex = '([0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12})'

# Get current power plan GUID
$currPlan = powercfg -getactivescheme
Write-Output "`$currPlan: `"$currPlan`""
$currPlanGUID = ($currPlan -split $guidRegex)[1]
Write-Output "`$currPlanGUID: `"$currPlanGUID`""

# Determine if the high performance power plan exists
# Necessary because it was removed from laptops around 1709 in favor of some performance slider bullshit
Write-Output "Checking if high performance plan exists..."

# Get list of power plans
$plans = powercfg -l
Write-Output "Performance plans:"
Write-Output $plans
Write-Output " "

$highPlanGUID = 'none'
Write-Output "Looping through performance plans..."
foreach($plan in $plans) {
	Write-Output "Checking plan: `"$plan`""
	if($plan.contains("High performance")) {
		Write-Output "Plan contains `"High performance`"."
		$highPlan = $plan
		Write-Output "`$highPlan: `"$highPlan`""
		$highPlanGUID = ($highPlan -split $guidRegex)[1]
		Write-Output "`$highPlanGUID: `"$highPlanGUID`""
		break
	}
	else {
		Write-Output "Plan does not contain `"High performance`"."
	}
}

# If the high performance plan exists, use it
if($highPlanGUID -ne 'none') {
	Write-Output "High performance plan exists."
	
	Write-Output "Testing if current plan is high performance plan..."
	if ($currPlanGUID -ne $highPlanGUID) {
		Write-Output "Current plan is NOT high performance plan."
		
		Write-Output "Setting current plan to high performance plan with `"powercfg -setactive $highPlanGUID`"..."
		Try {
			powercfg -setactive $highPlanGUID
			Write-Output "Done."
		}
		Catch {
			Write-Output "Unable to set current plan to high performance plan."
			dumpError $_
		}
	}
	else {
		Write-Output "Current plan is already high performance plan."
	}
}
# Otherwise, just change the sleep settings to "Never"
else {
	Write-Output "High performance plan does not exist."
	Write-Output "Setting sleep settings of current plan to `"Never`""
	# Plugged in
	Try {
		powercfg -change -standby-timeout-ac 0
		Write-Output "Done."
	}
	Catch {
		Write-Output "Unable to change plugged in sleep setting."
		dumpError $_
	}
	# On battery
	Try {
		powercfg -change -standby-timeout-dc 0
		Write-Output "Done."
	}
	Catch {
		Write-Output "Unable to change battery sleep setting."
		dumpError $_
	}
	
	# Also change the "close lid" action to none
	# https://docs.microsoft.com/en-us/windows-hardware/design/device-experiences/powercfg-command-line-options#option_setacvalueindex
	# powercfg  /setacvalueindex  scheme_GUID  sub_GUID  setting_GUID  setting_index
	Write-Output "Setting `"close lid`" actions of current plan to `"None`""
	
	# Plugged in
	Try {
		powercfg -setacvalueindex SCHEME_CURRENT SUB_BUTTONS LIDACTION 0
		Write-Output "Done."
	}
	Catch {
		Write-Output "Unable to change plugged in lid close action."
		dumpError $_
	}
	
	# On battery
	<#
	Try {
		powercfg -setdcvalueindex SCHEME_CURRENT SUB_BUTTONS LIDACTION 0
		Write-Output "Done."
	}
	Catch {
		Write-Output "Unable to change battery lid close action."
		dumpError $_
	}
	#>
}

Write-Output "EOF"


