﻿# https://helpx.adobe.com/creative-suite/kb/disable-auto-updates-application-manager.html
New-Item "${ENV:CommonProgramFiles(x86)}\Adobe\AAMUpdaterInventory\1.0\AdobeUpdaterAdminPrefs.dat" -ItemType File -Force
$settings = Get-Content "${ENV:CommonProgramFiles(x86)}\Adobe\AAMUpdaterInventory\1.0\AdobeUpdaterAdminPrefs.dat"
$settings = "<?xml version=`"1.0`" encoding=`"UTF-8`" ?>
`n
`n<Preferences>
`n
`n<Suppressed>0</Suppressed>
`n
`n</Preferences>"
$settings | Set-Content "${ENV:CommonProgramFiles(x86)}\Adobe\AAMUpdaterInventory\1.0\AdobeUpdaterAdminPrefs.dat"

# http://www.adobe.com/devnet-docs/acrobatetk/tools/PrefRef/Windows/Updater-Win.html?zoom_highlight=updates#Updater(basicsettings)
reg add "HKLM\SOFTWARE\Policies\Adobe\Adobe Acrobat\DC\FeatureLockDown" /v "bUpdater" /t "REG_DWORD" /d "1" /f
