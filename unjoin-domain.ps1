param(
[string]$user="No user given",
[string]$pass="No pass given",
[string]$workgroup="Workgroup"
)

Set-PSDebug -Trace 1

$pass2 = ConvertTo-SecureString $pass -AsPlainText -Force
$creds = New-Object System.Management.Automation.PSCredential ($user, $pass2)
add-computer -workgroupname $workgroup -credential $creds -localcredential $creds -force -passthru -verbose