param(
	[string]$message='No message given',
	# [string]$channel,
	[string]$title='No title given',
	# [string]$icon,
	[Parameter(Mandatory=$true)][string]$webhook
)

Write-Output "-----------------"
Write-Output "PSVersionTable:"
Write-Output $PSVersionTable
# Write-Output "channel: $channel"
Write-Output "message: $message"
Write-Output "title: $title"
# Write-Output "icon: $icon"
Write-Output "webhook: $webhook"

# Build payload

#Translate emojis from Slack to MSTeams equivalent (Added for testing the script with prod, temporarily)
if ($message.Contains(":arrow_forward:")){
	$message = $message.Replace(":arrow_forward: ", "")
	$title = "&#x25b6; " + $title
}

if ($message.Contains(":pacman:")){
	$message = $message.Replace(":pacman: ", "")
	$title = "&#x1F504; " + $title
}

if ($message.Contains(":green_checkmark:")){
	$message = $message.Replace(":green_checkmark: ", "")
	$title = "&#x2705; " + $title
}

if ($message.Contains("*")){
	$message = $message.Replace("*", "**")
}

$payload = @{}
$payload['title'] = $title
$payload['text'] = $message

Write-Output "payload:"
Write-Output "$payload"

# Send REST call
# https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/invoke-webrequest?view=powershell-5.1
#Invoke-WebRequest -Body (ConvertTo-Json -Compress -InputObject $payload) -UseBasicParsing -Method Post -Uri $webhook
Invoke-WebRequest `
	-Body (ConvertTo-Json -Compress -InputObject $payload) `
	-UseBasicParsing `
	-Method Post `
	-Uri $webhook `
	-ContentType "application/json" 

Write-Output "-----------------"
