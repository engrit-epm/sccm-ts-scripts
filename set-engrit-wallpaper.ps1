﻿param(
	[string]$Source
)

# Determine OS version
$os = [Environment]::OSVersion.Version.Major

if($os -eq "6") {
	Write-Output "Detected Windows 7 (major version $os)."
}
elseif($os -eq "10") {
	Write-Output "Detected Windows 10 (major version $os)."
}
else {
	Write-Output "Unrecognized Windows version: $os"
}

# Download source files
$local = "c:\engrit\scripts"
Copy-Item "$Source\Wallpaper" $local -Recurse

# Windows 7 and 10
$filePath = "C:\Windows\WEB\Wallpaper\Windows\"
$fileName = "img0.jpg"
$file = $filePath + $fileName

$fileNewPath = "$local\Wallpaper\DefaultRes\"
$fileNewName = "img0.jpg"
$fileNew = $fileNewPath + $fileNewName

# Take ownership of the files
TAKEOWN /f $file
# Set permissions for SYSTEM Account
ICACLS $file /Grant 'system:(F)'
# Delete existing files
Remove-Item $file
# Copy custom file
Copy-Item $fileNew $file

# Windows 10 only
if ($os -eq "10") {
	
	$filePath = "C:\Windows\Web\4K\Wallpaper\Windows\"

	$fileNameAll = "*.*"
	$fileAll = $filePath + $fileNameAll

	$fileName = "img0.jpg"
	$file = $filePath + $fileName

	$fileNewPath = "$local\Wallpaper\4k\"
	$fileNewName = "img0.jpg"
	$fileNew = $fileNewPath + $fileNewName

	# Take ownership of the files
	TAKEOWN /f $fileAll
	# Set permissions for SYSTEM Account
	ICACLS $fileAll /Grant 'system:(F)'
	# Delete existing files
	Remove-Item $fileAll
	# Copy custom file
	Copy-Item $fileNew $file
}

Write-Output "Done."